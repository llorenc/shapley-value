# shapley-value

Shapley value Monte Carlo simulation with R

We estimate relative error of Monte Carlo simulation using the Central Limit Theorem and a Student distribution of the samples.

Monte Carlo is run until the error is lower than a threshold with a given probability.

